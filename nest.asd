(defsystem nest
  :version (:read-file-form "version.sexp")
  :author "Matt Novenstern"
  :license "MIT"
  :depends-on ("beaver"
               "cl-reexport"
               "cl-smtp"
               "clack"
               "clack-static-asset-middleware"
               "djula"
               "jonathan"
               "lack-request"
               "local-time"
               "sanity-clause"
               "trivia"
               "trivial-mimes"
               "woo")
  :components ((:module "src"
                :components
                ((:file "utils")
                 (:file "route/trie")
                 (:file "route")
                 (:file "content-type")
                 (:file "request")
                 (:file "response")
                 (:file "configuration")
                 (:file "app")
                 (:file "static")
                 (:file "session")
                 (:file "templates")
                 (:file "djula-patch")
                 ;; (:file "mail")
                 (:file "auth")
                 (:file "nest"))))
  :description "A simple web framework built on clack."
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.markdown"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op nest-test))))

(defsystem nest/generate
  :pathname "src/generate"
  :depends-on ("quickproject")
  :components ((:file "generate")))

(defsystem nest/integrations/postmodern
  :pathname "src/integrations/postmodern"
  :depends-on ("beaver"
               "nest"
               "postmodern"
               "cl-postgres+local-time")
  :components ((:file "postmodern")
               (:file "mixins")))

(defsystem nest/middlewares/beaver
  :depends-on ("beaver"
               "beaver/st-json"
               "beaver/ansi-term"
               "nest"
               "uuid")
  :pathname "src/middlewares"
  :components ((:file "beaver")))

(defsystem nest/middlewares/csrf
  :depends-on ("postmodern"
               "nest")
  :pathname "src/middlewares"
  :components ((:file "csrf")))

(defsystem nest/middlewares/postmodern
  :depends-on ("postmodern"
               "nest"
               "nest/integrations/postmodern")
  :pathname "src/middlewares"
  :components ((:file "postmodern")))

(defsystem nest/pubsub
  :depends-on ("nest")
  :pathname "src/pubsub/"
  :components ((:file "package")))

(defsystem nest/pubsub/redis
  :depends-on ("nest/pubsub"
               "cl-redis")
  :pathname "src/pubsub/"
  :components ((:file "redis")))

(defsystem nest/server-sent-events
  :depends-on ("nest")
  :pathname "src/server-sent-events/"
  :components ((:file "package")))
