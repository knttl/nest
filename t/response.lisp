(defpackage #:nest-test.response
  (:use #:cl #:alexandria #:prove))

(in-package #:nest-test.response)

(plan 1)

(subtest "response"
  (subtest "respond"

    (let ((resp (nest.response:respond "Hello" :format :text :status 403)))
      (is (first resp) 403 "Status code is correct.")
      (is (second resp) '(:content-type "text/plain" :content-length 5) "Headers are correct.")
      (is (third resp) '("Hello") "The response body is in a list."))))
