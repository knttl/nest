(defpackage #:nest-test.route.trie
  (:use #:cl #:prove))

(in-package #:nest-test.route.trie)


(plan 1)

(subtest "nest.route.trie"
  (subtest "maybe-chunk"
    (is-values  (nest.route.trie::maybe-chunk ":something/") '(t "something" "/")
        "Finds parameters behind :")
    (is-values (nest.route.trie::maybe-chunk ":place.:extension") '(t "place" ".:extension")
        "Stops reading parameters at .")
    (is-values (nest.route.trie::maybe-chunk "place.:extension") '(nil "place." ":extension")
        "Doesn't find parameters when there are none")

    (is-values (nest.route.trie::maybe-chunk ":place.:extension") '(t "place" ".:extension")
               "Remaining string is cut correctly.")
    (is-values (nest.route.trie::maybe-chunk "/:name") '(nil "/" ":name")
               "Handles chunks with just slashes."))

  (subtest "predicates"
    (let ((capture-node (nest.route.trie::make-trie :node "*" :parameter-name :pasta))
          (leaf-node    (nest.route.trie::make-trie)))

      (ok (nest.route.trie::capture-p capture-node)
          "Capture nodes recognized")

      (ok (nest.route.trie::leaf-p    leaf-node)
          "Leaf nodes recognized")))

  (subtest "match-p"
    (let ((capture-node (nest.route.trie::make-trie :node "*" :parameter-name :potato))
          (text-node    (nest.route.trie::make-trie :node "potato/")))

      (is-values (nest.route.trie::match-p capture-node "pasta/1") '("pasta" "/1")
                 "Match capture before a slash works")

      (is-values (nest.route.trie::match-p capture-node "something.jpg") '("something" ".jpg")
                 "Match capture before a dot works")

      (is-values (nest.route.trie::match-p text-node "potato/yukon") '(t "yukon")
                 "Text match works")

      (is (nest.route.trie::match-p text-node "broccoli/yum") nil
          "Text fails correctly")))

  (subtest "build-trie"
    (ok (nest.route.trie:build-trie
         '(("/pizza/show" . print) ("/pizza/yes" . print)
           ("/pasta" . print) ("/pasta/:name" . print)
           ("/pasta/:name/edit" . print) ("/pizza/" . print)))
        "Builds a nonsense trie")

    (let ((trie (nest.route.trie:build-trie
                 '(("/pizza/show" . print) ("/pizza/yes" . print)
                   ("/pasta" . print) ("/pasta/:name" . print)
                   ("/pasta/:name/edit" . print) ("/pizza/" . print)))))
      (is (length (nest.route.trie::trie-branches (first (nest.route.trie::trie-branches trie))))
          2
          "There are only one `izza/` and `asta/` node under `/p`."))

    (let ((trie (nest.route.trie::build-trie
                 '(("/pizza/show" . show) ("/pizza/show" . yes)))))
      (is (length (nest.route.trie::trie-branches trie)) 1
          "Routes get overridden, not duplicated."))

    (let ((trie (nest.route.trie:build-trie
                 '(("/api/user" . something)
                   ("/api/user/verify" . verify)))))
      (is (length (nest.route.trie::trie-branches
                   (car (nest.route.trie::trie-branches trie))))
          1
          "Subroute with leading '/' gets put in the right place (as a sub-route)"))

    (ok (nest.route.trie:build-trie
         '(("/" . print) ("/about" . print)
           ("/favicon.ico" . print)))
        "Builds a tree with little in common. (This used to freeze trie generation)"))

  (subtest "find-node"
    (let ((trie (nest.route.trie::build-trie
             '(("/pizza/show" . print) ("/pizza/yes" . some-symbol)
               ("/pasta" . print) ("/pasta/:name" . print)
               ("/pasta/:name/edit" . print) ("/pizza/" . print)))))

      (ok (nest.route.trie:find-node trie "/pizza/yes")
          "Matches a route")

      (is (nest.route.trie:find-node trie "/pizza/no") nil
          "Doesn't match an undefined route")

      (is (multiple-value-bind (node function captures)
              (nest.route.trie:find-node trie "/pasta/penne/edit")
            (getf captures :name))
          "penne"))))

(finalize)
