(in-package :cl-user)
(defpackage nest-test
  (:use :cl
        :nest
        :prove))
(in-package :nest-test)

;; NOTE: To run this test file, execute `(asdf:test-system :nest)' in your Lisp.

(plan nil)

;; blah blah blah.

(finalize)
