(defpackage #:nest-test.configuration
  (:use #:cl #:prove))

(in-package #:nest-test.configuration)

(plan 1)

(defmacro with-env (bindings &body body)
  (let ((vars (loop for (var value) in bindings
                 collect (list (gensym (string-upcase var)) (string-upcase var) value))))

    `(let ,(loop for (sym var value) in vars
              collect `(,sym (uiop:getenv ,var)))
       (unwind-protect
            (progn

              ;; Set new environment values
              ,@(loop for (sym var value) in vars
                   collect `(setf (uiop:getenv ,var)
                                  ,(cond
                                     ((null value) "")
                                     (t (format nil "~a" value)))))

                   ;; Body
                     ,@body)

              ;; Cleanup - reset variables
              (progn
                ,@(loop for (sym var val) in vars
                     collect `(setf (uiop:getenv ,var)
                                    (cond
                                      ((null ,sym) "")
                                      (t (format nil "~a" ,sym))))))))))

(subtest "config-environment"
  (subtest "default"
    (is (nest.configuration::config-environment) :development))

  (subtest "environment variables"

    (with-env ((ENV :production))
      (is (nest.configuration::config-environment) :production))

    (with-env ((ENV :development))
      (is (nest.configuration::config-environment) :development))

    (with-env ((ENV :test))
      (is (nest.configuration::config-environment) :test)))

  (subtest "production environment"
    (with-env ((ENV :production)
               (AWS_SECRET "potato"))
      (nest.configuration::clear-config)
      (is (nest:config :aws-secret) "potato"
          "Configuration is read from environment in production mode"))))

(finalize)
