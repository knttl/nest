(defpackage #:nest.crud
  (:use #:cl #:alexandria #:nest.route))

(in-package #:nest.crud)

(defmacro defcrud (class))
