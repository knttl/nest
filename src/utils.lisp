(defpackage nest.utils
  (:use :cl)
  (:export
   #:deprecated))

(in-package :nest.utils)

(defmacro deprecated (function &key replacement since)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (fdefinition ',function)
           (lambda (&rest args)
             (,(if replacement 'warn 'error)
              ,(format nil "~a is deprecated~@[ since ~a~]~@[, use ~a instead~]."
                       function
                       since
                       replacement))
             ,(when replacement `(apply ',replacement args))))))
