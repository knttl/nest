(in-package #:parser-combinators)

;;; something in the parser-combinator package blows up inside djula
;;; when accessing a hash table from multiple threads.  Making the
;;; hash table synchronized on sbcl is a quick way to work around it
;;; but isn't portable to other implementations.

#+sbcl
(defclass context-common ()
  ((length        :accessor length-of        :initarg :length        :initform 0)
   (front         :accessor front-of         :initarg :front         :initform (make-instance 'context-front))
   (cache         :accessor cache-of         :initarg :cache         :initform nil)
   (seen-postions :accessor seen-postions-of :initarg :seen-position :initform (make-hash-table :synchronized t))))

#+sbcl
(defvar *parser-cache* (make-hash-table :synchronized t))
