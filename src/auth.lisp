(defpackage #:nest.auth
  (:use #:cl)
  (:import-from #:nest.session
                #:session)
  (:export #:userid
           #:login
           #:logout
           #:logged-in-p))

(in-package #:nest.auth)

(defun userid ()
  (gethash :user-id (session)))

(defun login (id)
  (setf (gethash :user-id (session)) id))

(defun logged-in-p ()
  (when (gethash :user-id (session)) t))

(defun logout ()
  (remhash :user-id (session)))
