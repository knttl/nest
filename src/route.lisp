(defpackage #:nest.route
  (:use #:cl #:alexandria)
  (:export #:make-routing-table
           #:routing-table
           ;; Types
           #:http-method
           #:find-route)
  (:documentation ""))

(in-package #:nest.route)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (defparameter +methods+ '(:GET :POST :PATCH :DELETE :PUT :OPTION :HEAD)
    "HTTP methods as keywords."))

(deftype routing-table () 'hash-table)
(deftype http-method () `(member ,@+methods+))

(defun make-routing-table ()
  (let ((new-table (make-hash-table :test 'eq)))

    (dolist (method +methods+)
      (setf (gethash method new-table) (nest.route.trie::make-trie)))

    new-table))

(defun name-route (slug methods)
  (format nil "ROUTE-~S-~{~A~^-~}" slug (sort methods 'string>)))


(defun find-route (routing-table env)
  (let ((method (getf env :request-method))
        (uri    (or (getf env :path-info) "/")))
    (nest.route.trie:find-node (gethash method routing-table) uri)))
