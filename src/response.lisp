(defpackage #:nest.response
  (:use #:cl #:alexandria)
  (:local-nicknames (:content-type :nest.content-type)
                    (:request :nest.request))
  (:export #:respond
           #:respond-json
           #:redirect
           #:content-typecase))

(in-package #:nest.response)


(defmacro content-typecase (&body body)
  "Matches content type symbols as defined in :variable:`nest.content-type:+mime-types+`.  The first rule that matches, wins, so clauses should be ordered in the 'preference' of mime types the server would like to use.  A catchall `T` clause can be used to default to certain behavior as well."

  (alexandria:with-gensyms (accept-header)
    `(let ((,accept-header (request:header "accept")))
       (flet ((match-any (type)
                (if (string= ,accept-header "*/*")
                    ;; In the event of a wild accept header, the first rule wins
                    t
                    (loop :for typestring :in (cdr (assoc type content-type:+mime-types+))
                          :thereis (ppcre:scan typestring ,accept-header)))))
         (cond
           ,@(loop :for (type . clause-body) :in body
                   :unless (or (eql t type) (assoc type content-type:+mime-types+))
                     :do (warn "mime-type ~s not recognized." type)
                   :collect (list* (if (eq type t) t `(match-any ,type)) clause-body)))))))


(defun respond (body &key (format :html) (status 200) headers)
  "Generates a clack-compatible response list."

  (list
   status

   (append headers
           (unless (eq format :none)
             (list :content-type (content-type:format-to-mime format)))
           ;; Calculate the content length if we know we can
           (when (stringp body) (list :content-length (babel:string-size-in-octets body))))

   (typecase body
     (string (list body))
     (otherwise body))))


(defun respond-json (body &key (status 200) headers)
  "Like #'respond but encodes 'body for you!"
  (respond (jojo:to-json body)
           :format :json
           :status status
           :headers headers))


(defun redirect (url &key (status 302) headers)
  "Send a response that redirects the client to `url`.  Status can only be `301` or `302`."
  (assert (= (floor status 100) 3))
  (respond ""
           :status status
           :headers (append headers `(:location ,url))))
