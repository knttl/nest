(defpackage #:nest.session
  (:use #:cl)
  (:export #:session))

(in-package #:nest.session)

(defmacro session ()
  '(getf (nest.request:environment) :lack.session))
