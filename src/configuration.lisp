(defpackage #:nest.configuration
  (:use #:cl #:alexandria)
  (:export #:config
           #:*testing*
           #:production-p
           #:development-p
           #:test-p))

(in-package #:nest.configuration)

(defparameter +valid-environments+ '(:production :test :development)
  "Values it is not an error to set `*environment*` to.")


(defvar *testing* nil
  "Set things to test mode.  Because somethinems you need to do it manually.")


(defvar *environment* nil
  "The environment the app is currently running in.  Should be `(member :production :test :development)`.  Will override the `ENV` environment variable, if set.")


(defparameter +base-configuration+
  '(:nest-address (:string :validator (:not-empty) :default "::0")
    :nest-worker-count (:integer :validator ((:int :min 1)) :default 1)
    :nest-port (:integer :validator ((:int :min 0)) :default 5000)
    :nest-server (:member :members (:woo :hunchentoot) :default :woo)
    :nest-cache-templates (:boolean :default nil)
    :env (:member :members (:production :test :development) :default :development))
  "Variables that nest uses that should be a part of every configuration implicitly.")


(let (config)
  (defun ensure-config ()
    (unless config
      (dotenv!)
      (setf config
            (sanity-clause:load
             (append
              (sanity-clause.loadable-schema:load-schema +base-configuration+)
              (sanity-clause.loadable-schema:load-schema
               (merge-pathnames #P"config/schema.sexp")))
             :env))))

  (defun config (key &optional default)
    (when default
      (warn "default passed to nest:config is deprecated and will be unused eventually."))
    (ensure-config)
    (let ((value (getf config key 'not-found)))
      (if (eq value 'not-found)
          (progn
            (warn "configuration key ~a not defined - add it to your app's schema.sexp" key)
            default)
          value)))

  (defun clear-config ()
    (setf config nil)))


(defun config-environment ()
  "Returns the running configuration environment based on the environment variable `ENV` and the variable `*environment*`.  The lisp variable will override the environment variable if both exist.  The default value is `:development`."

  (config :env))


(defun production-p ()
  (eq (config-environment) :production))


(defun test-p ()
  (eq (config-environment) :test))


(defun development-p ()
  (eq (config-environment) :development))


(defun expand-possibly-quoted-string (string)
  (flet ((expand-newlines (string)
           (ppcre:regex-replace-all "\\r"
                                    (ppcre:regex-replace-all "\\n" string (format nil "~a" #\Newline))
                                    (format nil "~a" #\Linefeed))))
    (or (ppcre:register-groups-bind (quote escaped-value) ("(['\"`])([\\s\\S]*)\\1$" string)
          (declare (ignore quote))
          ;; it was quoted in some way
          (if (and (not (zerop (length escaped-value)))
                   (char= (char escaped-value 0) #\"))
              ;; if it was double quoted, expand escaped newlines
              (expand-newlines escaped-value)
              escaped-value))
        ;; it wasn't quoted or was empty, so return original string
        string)))


(defun dotenv! (&key error-on-exists-p)
  (when (uiop:file-exists-p (merge-pathnames ".env"))
    (loop :for line :in (uiop:read-file-lines (merge-pathnames ".env"))
          :for (key value) := (str:split #\= line :limit 2)
          :for exists-p := (uiop:getenvp key)
          :do (cond
                ((and exists-p error-on-exists-p)
                 (error "environment variable ~S already defined before applying .env file." key))
                (t
                 (setf (uiop:getenv key) (expand-possibly-quoted-string value)))))))
