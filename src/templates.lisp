(defpackage #:nest.templates
  (:use #:cl #:alexandria)
  (:export #:render))

(in-package #:nest.templates)

(defvar *template-store* nil
  "Store for djula templates.")


(defmethod nest.app:start :before ((app nest.app::app) &rest args)
  (declare (ignore args))
  (setup-template-cache))


(defun setup-template-cache (&optional template-store)
  (setf *template-store*
        (or template-store
            (make-instance (if (nest.configuration:config :nest-cache-templates)
                               'djula:memory-template-store
                               'djula:filesystem-template-store)
                           :search-path (list (nest.app:app-relative-pathname "templates/"))))))


(defun render-template (name &rest args)
  (let ((djula:*current-store* *template-store*))
    (apply #'djula:render-template*
           name
           nil
           :session (nest.session:session)
           args)))


(defun render (name &rest args &key (status 200) &allow-other-keys)
  (declare (optimize speed space))
  (nest.response:respond
   (apply #'render-template name (remove-from-plist args :status))
   :status status))
