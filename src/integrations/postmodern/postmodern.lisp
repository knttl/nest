(defpackage :nest/integrations/postmodern
    (:use :cl)
    (:local-nicknames (:pomo :postmodern)
                      (:log :beaver))
    (:export
     #:with-connection
     #:init

     #:connect))

(in-package :nest/integrations/postmodern)

(defun init ()
  (setf local-time:*default-timezone* local-time:+utc-zone+)
  (local-time:set-local-time-cl-postgres-readers)

  (unless (nest:production-p)
    ;; Turn on echoing queries to the repl
    (setf cl-postgres:*query-log* t
          (symbol-function 'cl-postgres:log-query)
          (lambda (query time-units)
            (let ((duration (round (/ (* 1000 time-units)
                                      internal-time-units-per-second))))
              (log:with-fields (:duration duration integer
                                :query query string)
                (log:info "CL-POSTGRES query")))))))


(let (last-dsn connection-spec)
  (defun memoized-dsn-to-cl-postgres-connection-spec ()
    (let ((database-url (quri:uri (nest:config :database-url))))
      (if (and last-dsn (quri:uri= last-dsn database-url))
          ;; return memoized value
          connection-spec
          ;; calculate and store spec
          (setf last-dsn database-url
                connection-spec
                (with-slots ((proto quri.uri::scheme)
                             (auth quri.uri::userinfo)
                             (hostname quri.uri::host)
                             (port quri.uri::port)
                             (db quri.uri::path))
                    (quri:uri database-url)
                  (assert (string= proto "postgres")
                          (proto)
                          "'database-dsn' must be 'postgres' for nest/integrations/postmodern, but was ~a.")
                  (let ((colon-position (position #\: auth :test #'char=)))

                    (list (subseq db 1)
                          (subseq auth 0 colon-position)
                          (subseq auth (1+ colon-position))
                          hostname
                          :port (or port 5432)))))))))


(defun connect ()
  (apply #'pomo:connect-toplevel (memoized-dsn-to-cl-postgres-connection-spec)))


(defmacro with-connection (&body body)
  "Looks for database specified in configuration in database-uri and binds top-level postmodern *database* to that."
  `(pomo:with-connection (memoized-dsn-to-cl-postgres-connection-spec)
     ,@body))
