(defpackage :nest/integrations/postmodern.mixins
    (:use :cl)
    (:local-nicknames (:pomo :postmodern))
    (:export
     #:timestamp-mixin
     #:created-at
     #:updated-at
     #:id-mixin
     #:id))

(in-package :nest/integrations/postmodern.mixins)


(defclass id-mixin ()
  ((id :col-type bigserial
       :primary-key t
       :accessor id))
  (:metaclass pomo:dao-class)
  (:keys id))


(defclass timestamp-mixin ()
  ((updated-at :initform :null
               :type (or (eql :null) local-time:timestamp)
               :col-type (or pomo:db-null local-time:timestamp)
               :accessor updated-at)
   (created-at :initform (local-time:now)
               :col-type local-time:timestamp
               :accessor created-at))
  (:metaclass pomo:dao-class))


(defmethod initialize-instance :after ((o timestamp-mixin) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))

  (unless (slot-boundp o 'created-at)
    (setf (created-at o) (local-time:now))))


(defmethod postmodern:update-dao :before ((dao timestamp-mixin))
  (setf (updated-at dao) (local-time:now)))


(defmethod print-object ((object id-mixin) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "id=~a" (when (slot-boundp object 'id)
                             (id object)))))
