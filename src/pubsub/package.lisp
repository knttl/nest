(defpackage nest/pubsub
  (:use :cl)
  (:local-nicknames (:a :alexandria)
                    (:log :beaver))
  (:export
   #:broadcast
   #:make-channel
   #:start-manager
   #:stop-manager
   #:topic
   #:make-redis-pubsub-manager
   #:redis-manager
   #:restart-manager
   #:close-channel))

(in-package :nest/pubsub)


(defparameter +default-max-channels+ 2048)


(defclass manager ()
  ((max-channels
    :reader max-channels
    :initarg :max-channels
    :initform +default-max-channels+
    :documentation "Maximum number of channels to allow before signaling a `too-many-channels` condition.")
   (thread
    :accessor thread
    :documentation "Thread that the message broker runs in.")
   (subscriptions
    :accessor subscriptions
    :initform (make-hash-table :test 'equalp)
    :type hash-table
    :documentation "Map of topics to one or more channels."))
  (:documentation "Class for managing channels and keeping track of anything needed to run the configured pubsub implementation.  Perhaps connections to external services."))


(defclass channel ()
  ((manager :type manager
            :accessor manager
            :documentation "Manager responsible for managing the channel.")
   (topic :initarg :topic
          :reader channel-topic
          :documentation "Topic of the channel that messages will be sent on.")
   (message-callback :initarg :message-callback
                     :type (function (string channel) (values))
                     :reader message-callback
                     :documentation "Callback function for listening to messages.  Gets the message and also a function to close the channel."))
  (:documentation "Channel to receive for a given topic on. Not thread safe.  You can make a new instance for each thread you need to access messages from."))


(defmethod print-object ((object channel) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "with topic ~S" (channel-topic object))))


(defgeneric start-manager (manager)
  (:documentation "Starts the manager so it's ready to manage messages."))


(defgeneric stop-manager (manager)
  (:documentation "Stops the manager and performs any necessary cleanup."))


(defgeneric restart-manager (manager)
  (:documentation "Restarts the manager and performs any necessary cleanup.")
  (:method ((manager manager))
    (stop-manager manager)
    (start-manager manager)))


(defgeneric make-channel (manager topic &key message-callback)
  (:method ((manager manager) (topic string) &key message-callback)
    (let ((channel (make-instance 'channel
                                  :topic topic
                                  :message-callback message-callback)))
      (register-channel manager channel)))

  (:method ((manager manager) (subject t) &rest rest)
    (apply #'make-channel manager (topic subject) rest)))


(defgeneric topic (thing)
  (:documentation "Method for generating a topic (string) for the given object.  Things like a combination of class name and a unique ID whould be a suitable topic.")

  (:method ((channel channel))
    (channel-topic channel)))


(defgeneric register-channel (manager channel)
  (:method ((manager manager) (channel channel))
    (setf (manager channel) manager)
    (push channel (gethash (topic channel) (subscriptions manager)))))


(defgeneric unregister-channel (manager channel)
  (:documentation "Called when a channel is disconnected for whatever reason.")

  (:method ((manager manager) (channel channel))
    (rplacd)))


(defgeneric broadcast (manager topic message)
  (:documentation "Send a message through the pubsub system."))


(defgeneric deliver-to-channels (manager topic message)
  (:method ((manager manager) (topic string) message)
    (dolist (channel (gethash topic (subscriptions manager)))
      (deliver channel message))))


(defgeneric deliver (channel message)
  (:documentation "Send a message on the channel.")

  (:method ((channel channel) message)
    (handler-bind ((error (lambda (condition)
                            (log:error channel message condition "Error while processing message" )
                            t)))
      (funcall (message-callback channel) message channel))))


(defgeneric close-channel (channel)
  (:documentation "Method to disconnect the channel from the manager and stop it from receiving events.")

  (:method ((channel channel))
    ;;; FIXME: Not thread safe
    (with-accessors ((manager manager) (topic topic)) channel
      (setf (gethash topic (subscriptions manager))
            (delete channel (gethash topic (subscriptions manager))
                    :count 1)))))
