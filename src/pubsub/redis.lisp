(in-package #:nest/pubsub)


(defclass redis-manager (manager)
  ((connection-dsn
    :initarg :connection-dsn
    :type string
    :reader connection-dsn)
   (event-loop-mutex
    :type bordeaux-threads:lock
    :initform (bordeaux-threads:make-lock "redis manager event-loop lock")
    :reader event-loop-mutex)
   (event-loop
    :type (or null bordeaux-threads:thread)
    :accessor event-loop
    :documentation "The thread running the event subscriber.  It's responsible for listening to redis events and sending them to channels.")
   (should-shutdown-p
    :type boolean
    :initform nil
    :accessor should-shutdown-p
    :documentation "Indicates the event loop should shutdown the next time it checks.")))


(defmethod start-manager ((manager redis-manager))
  (setf (subscriptions manager) (make-hash-table :test 'equalp)
        (event-loop manager)
        (bordeaux-threads:make-thread
         (lambda ()
           (bordeaux-threads:with-lock-held ((event-loop-mutex manager))
             (let ((dsn (quri:uri (connection-dsn manager))))
               (redis:with-connection (:host (quri:uri-host dsn)
                                       :port (quri:uri-port dsn)
                                       :auth (a:when-let ((auth (quri:uri-userinfo dsn)))
                                               (subseq auth (1+ (position #\: auth)))))
                 ;; subscribe to everything, only pass along what channels are
                 ;; subscribed to.
                 (red:psubscribe "*")

                 (loop :for raw-message := (redis:expect :anything :timeout 2)
                       :do (log:debug "Loop de loop")
                       :when (should-shutdown-p manager) :do (return)

                       :when raw-message
                         :do (destructuring-bind (event subscription topic message) raw-message
                               (log:debug event subscription topic message "Got pubsub message.")

                               (deliver-to-channels manager topic message)))

                 (setf (should-shutdown-p manager) nil)))))
         :name (format nil "Event loop for ~a" manager))))


(defmethod stop-manager ((manager redis-manager))
  (when (and (not (null (event-loop manager)))
           (bordeaux-threads:thread-alive-p (event-loop manager)))
    (setf (should-shutdown-p manager) t)
    ;; block until the event loop shuts down
    (bordeaux-threads:acquire-lock (event-loop-mutex manager))
    (bordeaux-threads:release-lock (event-loop-mutex manager))))


(defmethod broadcast ((manager redis-manager) topic message)
  (let ((dsn (quri:uri (connection-dsn manager))))
    (redis:with-connection (:host (quri:uri-host dsn)
                            :port (quri:uri-port dsn)
                            :auth (a:when-let ((auth (quri:uri-userinfo dsn)))
                                    (subseq auth (1+ (position #\: auth)))))
      (red:publish topic message))))
