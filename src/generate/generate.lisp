(defpackage :nest/generate
    (:use :cl)
    (:export
     #:generate-app))

(in-package :nest/generate)

(defun generate-app (name &key (root-directory (uiop:getcwd)) &aux (directory (merge-pathnames (make-pathname :directory `(:relative ,name)) root-directory)))
  (quickproject:make-project directory
                             :name name
                             :template-directory (asdf:system-relative-pathname :nest/generate "src/generate/template/")))
