(defpackage (#| TMPL_VAR name |#)/models
  (:use :cl :postmodern)
  (:local-nicknames (:mixins :nest/integrations/postmodern.mixins))
  (:import-from :nest/integrations/postmodern.mixins
                #:id
                #:created-at
                #:updated-at)

  (:export
   #:id
   #:created-at
   #:updated-at))

(in-package :(#| TMPL_VAR name |#)/models)
