(defpackage :(#| TMPL_VAR name |#)
  (:use :cl)
  (:local-nicknames (:log :beaver)
                    (:models :(#| TMPL_VAR name |#)/models))
  (:export
   #:app))

(in-package :(#| TMPL_VAR name |#))

(nest:defapp (#| TMPL_VAR name |#)-app
  :middlewares (nest/middlewares/beaver:+beaver-middleware+
                (lack.middleware.session:*lack-middleware-session*
                 :store (multiple-value-bind (proto auth hostname port)
                            (quri:parse-uri (nest:config :redis-url "redis://127.0.0.1:6379"))
                          (declare (ignore proto port))

                          (lack.session.store.redis:make-redis-store
                           :connection (redis:connect
                                        :host hostname
                                        :auth (when auth (subseq auth (1+ (position #\: auth))))))))
                nest/middlewares/postmodern:+postmodern-middleware+
                nest/middlewares/csrf:+csrf-middleware+))

(nest:defroute app "/"
  (nest:render "index.html"))
