(defsystem (#| TMPL_VAR name |#)
  :author "(#| TMPL_VAR author |#)"
  :depends-on ("alexandria"
               "beaver"
               "lack-middleware-session"
               "lack-session-store-redis"
               "nest"
               "nest/middlewares/beaver"
               "nest/middlewares/csrf"
               "nest/middlewares/postmodern"

               "(#| TMPL_VAR name |#)/models")
  :pathname "src"
  :components ((:file "app")))

(defsystem (#| TMPL_VAR name |#)/models
  :pathname "src/models"
  :depends-on ("cl-postgres+local-time"
               "postmodern"
               "postmodern-passenger-pigeon")
  :components ((:file "package")))
