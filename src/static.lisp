(defpackage #:nest.static
  (:use #:cl #:alexandria)
  (:import-from #:nest.response
                #:respond)
  (:import-from #:nest.app
                #:app-relative-pathname)
  (:import-from #:trivial-mimes
                #:mime)
  (:import-from #:local-time
                #:format-rfc1123-timestring)
  (:export #:serve-file))


(in-package #:nest.static)

(defun serve-file (path)
  "Serve a single file.  This is dangerous!!  Some attempts will be made to make sure that requests cannot escape the app directory, but this is not audited and you should really only use this at your own risk.  Probably safe for paths you define yourself, though."

  ;; based heavily on https://github.com/fukamachi/lack/blob/master/src/app/file.lisp#L55

  (let* ((path (app-relative-pathname (pathname path)))
         (content-type (mime path "text/plain"))
         (last-modified (format-rfc1123-timestring
                         nil
                         (local-time:universal-to-timestamp
                          (or (file-write-date path)
                              (get-universal-time))))))

    (with-open-file (body path
                          :direction :input
                          :if-does-not-exist nil)
      (let ((headers (list
                      :content-type content-type
                      :last-modified last-modified
                      :content-length (file-length body))))
        (respond path :headers headers :format :none)))))
