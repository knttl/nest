(defpackage nest/server-sent-events
  (:use :cl)
  (:local-nicknames (:log :beaver))
  (:export
   #:with-sse-response))

(in-package :nest/server-sent-events)

(defgeneric write-headers (socket headers)
  (:method :before (socket headers)
    (log:debug "writing headers"))

  (:method (socket headers)
    (flet ((write-data (string)
             (clack.socket:write-sequence-to-socket
              socket
              (babel:string-to-octets
               string
               :encoding :ascii))))

      (write-data (format nil "HTTP/1.1 200 OK~C~C"
                          #\Return
                          #\Newline))

      (loop :for (header value) :on headers :by #'cddr
            :do (write-data (format nil "~:(~A~): ~A~C~C"
                                    header
                                    value
                                    #\Return
                                    #\Newline)))

      (write-data (format nil "~C~C"
                          #\Return
                          #\Newline))))

  (:method :around ((socket woo.ev.socket:socket) headers)
    (wev:with-async-writing (socket :force-streaming t)
      (call-next-method))))


(defgeneric write-message (socket message &optional event-name)
  (:method :before (socket message &optional event-name)
    (declare (ignore socket))
    (log:debug message event-name "Writing message to socket"))

  (:method (socket message &optional (event-name "message"))
    (clack.socket:write-sequence-to-socket
     socket
     (babel:string-to-octets
      (format nil
              "event: ~a~C~C"
              event-name
              #\Return
              #\Linefeed)))

    (with-input-from-string (stream message)
      (loop :for line := (read-line stream nil :eof)
            :until (eq line :eof)
            :do (clack.socket:write-sequence-to-socket
                 socket
                 (babel:string-to-octets
                  (format nil "data: ~a~C~C"
                          line
                          #\Return
                          #\Linefeed)))))

    (clack.socket:write-sequence-to-socket
     socket
     (babel:string-to-octets
      (format nil "~C~C"
              #\Return
              #\Linefeed))))

  (:method :around ((socket woo.ev.socket:socket) stream &optional event-name)
    (declare (ignore stream event-name))
    (wev:with-async-writing (socket :force-streaming t)
      (call-next-method))))


(defmacro with-sse-response ((&key send-message close extra-response-headers) &body body)
  (alexandria:with-gensyms (socket responder)
    ;; Get underlying socket
    `(let ((,socket (getf (nest:environment) :clack.io)))
       (log:debug "Establishing SSE session")
       (flet ((,send-message (message &key (event-name "message"))
                (write-message ,socket message event-name))
              ,@(when close
                  `((,close ()
                             (clack.socket:flush-socket-buffer ,socket)
                             (clack.socket:close-socket ,socket)))))
         (unless ,socket
           (error "The current backend is unsupported for ~_
                   server-sent-events because it doesn't expose ~_
                   :clack.io in the environment."))

         (lambda (,responder)
           (declare (ignore ,responder))
           (write-headers ,socket `(:date ,(local-time:to-rfc1123-timestring (local-time:now))
                                    :connection "keep-alive"
                                    :content-type "text/event-stream"
                                    :cache-control "no-store"
                                    ,,@extra-response-headers))
           ,@body)))))
