(defpackage #:nest.request
  (:use #:cl #:alexandria)
  (:export #:body-parameter
           #:query-parameter
           #:parameter
           #:header

           #:with-body-params
           #:with-query-params
           #:with-params
           #:route-parameters
           #:with-environment
           #:environment
           #:ensure-request
           #:lambda-with-request-context))

(in-package #:nest.request)


(defvar *environment* nil
  "Dynamically bound on request.  The clack environment.")


(defun route-parameters ()
  (getf *environment* :route-parameters))


(defmacro with-environment ((environment &key route-parameters) &body body)
  `(call-with-environment ,environment (lambda () ,@body) :route-parameters ,route-parameters))


(defun call-with-environment (environment function &key route-parameters)
  (let ((*environment* (append environment (when route-parameters
                                             (list :route-parameters route-parameters)))))
    (funcall function)))


(defun environment ()
  *environment*)


(defun ensure-request ()
  (or (getf *environment* :request)
      (setf (getf *environment* :request) (lack.request:make-request *environment*))))

(defun keywordize (alist)
  (loop for (key . value) in alist
     appending (list (make-keyword (string-upcase key)) value)))

(defun body-parameter (key &optional default)
  (getf (keywordize (lack.request:request-body-parameters (ensure-request))) key default))

(defun query-parameter (key &optional default)
  (getf (keywordize (lack.request:request-query-parameters (ensure-request))) key default))

(defun parameter (key &optional default)
  (getf (keywordize
         (append (lack.request:request-body-parameters (ensure-request))
                 (lack.request:request-query-parameters (ensure-request))))
        key
        default))


(defun header (key &optional default)
  (gethash key (getf *environment* :headers)))


;; (defmacro with-with-params (name source-fn)
;;   (with-gensyms (fn)
;;     `(let ((,fn ,source-fn))
;;        (defmacro ,name (params &body body)
;;          `(let ,(loop for param-spec in params
;;                    for (param default) = (ensure-list param-spec)
;;                    collecting `(,param (let ((value (,,fn ,(make-keyword param) ,default)))
;;                                          (if (equal value "")
;;                                              nil
;;                                              value))))
;;             ,@body)))))

(defmacro with-body-params (params &body body)
  `(let ,(loop for param-spec in params
            for (param default) = (ensure-list param-spec)
            collecting `(,param (let ((value (body-parameter ,(make-keyword param) ,default)))
                                  (if (equal value "")
                                      nil
                                      value))))
     ,@body))

(defmacro with-query-params (params &body body)
  `(let ,(loop for param-spec in params
            for (param default) = (ensure-list param-spec)
            collecting `(,param (let ((value (query-parameter ,(make-keyword param) ,default)))
                                  (if (equal value "")
                                      nil
                                      value))))
     ,@body))

(defmacro with-params (params &body body)
  `(let ,(loop for param-spec in params
            for (param default) = (ensure-list param-spec)
            collecting `(,param (let ((value (parameter ,(make-keyword param) ,default)))
                                  (if (equal value "")
                                      nil
                                      value))))
     ,@body))



(defmacro lambda-with-request-context ((&key (to-capture '(*environment*
                                                           #+lev-ev-full wev:*evloop*)))
                                       lambda-list
                                       &body body)
  "Captures important dynamic variable bindings from the request environment so it stays available when the lambda is invoked later."

  (let ((gensyms (loop :for variable :in to-capture
                       :collect (gensym (symbol-name variable)))))
    `(let (,@(loop :for variable :in to-capture
                   :for gensym :in gensyms
                   :collecting (list gensym variable)))
       (lambda ,lambda-list
         (let (,@ (loop :for variable :in to-capture
                        :for gensym :in gensyms
                        :collecting (list variable gensym)))
           ,@body)))))
