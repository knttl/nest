(defpackage :nest/middlewares/postmodern
  (:use :cl)
  (:local-nicknames (:nest/pomo :nest/integrations/postmodern))
  (:export
   #:+postmodern-middleware+))

(in-package :nest/middlewares/postmodern)

(defparameter +postmodern-middleware+
  (lambda (app &key pooled-p)
    (nest/pomo:init)
    (lambda (env)
      (nest/pomo:with-connection ()
        (funcall app env)))))
