(defpackage :nest/middlewares/beaver
  (:use :cl)
  (:export #:+beaver-middleware+
           #:*request-id*))

(in-package :nest/middlewares/beaver)


(defparameter *request-id* nil
  "UUID that should be unique to each request.")


(defparameter +beaver-middleware+
  (lambda (app
           &key
             result-on-error
             (init-beaver-p t)
             on-error-hook
             (handle-errors-p (not (nest:development-p))))
    (when init-beaver-p
      (setf beaver:*loggers*
            (list (make-instance (if (nest:production-p)
                                     (find-symbol "ST-JSON-LOGGER" :beaver/st-json)
                                     (find-symbol "ANSI-TERM-LOGGER" :beaver/ansi-term))))))
    (lambda (env)
      (let ((*request-id* (string-downcase (princ-to-string (uuid:make-v4-uuid)))))
        (beaver:with-fields (:request-id *request-id* string
                             :path (getf env :request-uri) string
                             :method (string (getf env :request-method)) string
                             :protocol (string (getf env :server-protocol)) string
                             :referer (gethash "referer" (getf env :headers)) string
                             :user-agent (gethash "user-agent" (getf env :headers)) string)
          (block nil
            (handler-bind ((error (lambda (condition)
                                    (beaver:with-backtrace condition
                                      (beaver:error "An unexpected error occurred."))
                                    (when on-error-hook
                                      (funcall condition env))
                                    (when handle-errors-p
                                      (return
                                        (if result-on-error
                                            (typecase result-on-error
                                              ((or symbol function)
                                               (funcall result-on-error env condition))
                                              (t result-on-error))
                                            (nest.response:respond "Internal Server Error"
                                                                   :status 500
                                                                   :format :text)))))))

              (let ((start-time (get-internal-real-time)))
                (lack.util:funcall-with-cb
                 app
                 env
                 (lambda (response)
                   (let ((duration (/ (* 1000 (- (get-internal-real-time) start-time)) internal-time-units-per-second)))
                     (beaver:with-fields (:status-code (car response) integer
                                          :duration duration single-float )
                       (beaver:info "Served request.")))
                   response))))))))))
