(defpackage :nest/middlewares/csrf
  (:use #:cl #:alexandria)
  (:import-from :ironclad
                :ascii-string-to-byte-array
                :byte-array-to-hex-string
                :digest-sequence
                :make-digest)
  (:import-from #:nest
                #:session)
  (:export #:+csrf-middleware+
           #:csrf-token))

(in-package #:nest/middlewares/csrf)

(defvar *env* nil)

(defparameter +csrf-middleware+
  (lambda (app)
    (lambda (env)
      (let ((*env* env))
        (if (or (not (danger-method-p env))
                (and (danger-method-p env) (valid-token-p env)))
            (funcall app env)
            '(403
              (:content-type "text/plain"
               :content-length 4)
              ("CSRF"))))))
  "CSRF protection middleware that looks for the token in either the X-CSRF-Token header or _csrf_token body parameter in all relevant requests.")

(defun danger-method-p (env)
  (member (getf env :request-method)
          '(:POST :PUT :DELETE :PATCH)
          :test #'eq))

(defun body-parameter-token (env)
  (let ((parameters-alist (lack.request:request-body-parameters (lack.request:make-request env))))
    (cdr (assoc "_csrf_token" parameters-alist :test 'string-equal))))

(defun valid-token-p (env)
  ;;  (maphash (lambda (k v) (format t "~&~a: ~a ~%" k v)) (getf env :headers))
  ;;  (maphash (lambda (k v) (format t "~&~a: ~a ~%" k v)) (getf env :lack.session))
  (when-let (csrf-token (gethash :csrf-token
                                 (getf env :lack.session)))
    (string= csrf-token (or (gethash "x-csrf-token"
                                     (getf env :headers))
                            (body-parameter-token env)))))

(djula::def-tag-compiler csrf-token ()
  "Return a random CSRF token."
  (lambda (stream)
    (princ
     (csrf-token)
     stream)))

(defun csrf-token ()
  "Return a random CSRF token."
  (if (session)
      (if-let (csrf-token (gethash :csrf-token (session)))
        csrf-token
        (setf (gethash :csrf-token (session)) (generate-random-id)))
      ""))

(defun generate-random-id ()
  "Generates a random token."
  (byte-array-to-hex-string
   (digest-sequence
    (make-digest :SHA1)
    (ascii-string-to-byte-array
     (format nil "~A~A"
      (random 1.0) (get-universal-time))))))
