(defpackage #:nest.app
  (:use #:cl #:alexandria)
  (:local-nicknames (:configuration :nest.configuration)
                    (:log :beaver)
                    (:request :nest.request))
  (:export #:defapp
           #:*current-app*
           ;; Control methods
           #:start
           #:stop
           #:halt

           #:app-add-route
           #:defroute
           #:route-parameter
           #:not-found

           #:app-relative-pathname))

(in-package #:nest.app)

(defvar *current-app* nil
  "The currently active app for any given request.  Should be a sub-app, if applicable.  Dynamically bound on request.")


(defclass app ()
  ((routing-table :type nest.route:routing-table
                  :initform (nest.route:make-routing-table)
                  :accessor routing-table)
   (handler :type clack.handler::handler
            :initform nil
            :accessor handler)
   (callable-app :accessor callable-app
                 :initform nil)
   (middlewares :type proper-list
                :accessor middlewares
                :initarg :middlewares)
   (subapps :type association-list
            :reader subapps
            :initarg :subapps)
   (app-package :accessor app-package
                :initarg :package))
  (:documentation "FIXME: This one is important!"))

(defmacro defapp (name &key subapps middlewares)
  `(progn
     (defclass ,name (nest.app::app) ())

     (defparameter ,(intern "APP" *package*)
       (make-instance ',name
                      :subapps ,subapps
                      :middlewares ',middlewares
                      :package *package*))))

(defun clack-app (app)
  "Create a callable closure and wrap it in any clack middlewares listed."
  (flet ((apply-middleware (mw app)
           (if (listp mw)
               (apply (eval (first mw)) app (mapcar #'eval (rest mw)))
               (funcall (eval mw) app))))

    (loop for (path . subapp) in (subapps app)
       do (setf (callable-app (eval subapp)) (clack-app (eval subapp))))

    (let ((callable-app (lambda (env)
                          (call app env))))

      (setf (callable-app app)
            (dolist (mw (reverse (middlewares app)) callable-app)
              (setf callable-app
                    (apply-middleware mw callable-app))))

      (callable-app app))))

(defun maybe-call-subapp (app env)
  (when (subapps app)

    (flet ((call-subapp (subapp mount-point sub-path)
             (let ((env (copy-list env)))
               (setf (getf env :script-info) mount-point
                     (getf env :path-info) sub-path)
               (funcall (callable-app subapp) env))))

      ;; Return nil if no subapp matches
      (dolist (subapp-def (subapps app) nil)
        (let ((mount-point (car subapp-def))
              (subapp      (cdr subapp-def)))

          (multiple-value-bind
                (match-p sub-path) (alexandria:starts-with-subseq mount-point (getf env :path-info)
                                                                  :return-suffix t)

            ;; Does the request path start with the mount-point for this subapp?
            ;; if so, pass the app a modified environment
            (when match-p
              (return-from maybe-call-subapp (call-subapp subapp mount-point sub-path)))))))))


(defgeneric call (app env)
  (:method ((app app) env)
    (let ((*current-app* app))
      (or (maybe-call-subapp app env)
          (multiple-value-bind (match-p funcallable parameters)
              (nest.route:find-route (routing-table app) env)
            (request:with-environment (env :route-parameters parameters)
              (if match-p
                  ;; Route exists
                  (funcall funcallable)
                  ;; No route found, 404!
                  (not-found app))))))))


(defgeneric not-found (app)
  (:method ((app app))
    (nest.response:respond "<!doctype html>
<head><title>404</title></head>
<body><h1>404 Not Found</h1>
<p>The address you entered was not found on this server.</p>
</body>"
                           :status 404)))

(defgeneric start (app &rest args)
  (:method ((app app) &rest args)
    (when (handler app)
      (clack:stop (handler app)))
    (setf (handler app)
          (apply #'clack:clackup
                 (clack-app app)
                 :address (configuration:config :nest-address)
                 :server (configuration:config :nest-server)
                 :port (configuration:config :nest-port)
                 ;; debug mode in test and development, not production
                 :debug (not (configuration:production-p))
                 :use-thread (not (configuration:production-p))
                 :worker-num (configuration:config :nest-worker-count)
                 :use-default-middlewares nil
                 (delete-from-plist args :server :port :debug :use-default-middlewares)))
    (values))
  (:documentation "Start the app with the current configuration."))


(defgeneric stop (app)
  (:method ((app app))
    (clack:stop (handler app))))

(nest.utils:deprecated halt
                       :replacement stop
                       :since "0.4.0")


(defun app-add-route (app slug methods function)
  (dolist (method methods)
    (declare (type nest.route:http-method method))
    (nest.route.trie:add-route (gethash method (routing-table app)) slug function))
  function)

(defmacro defroute (app slug-and-methods &body body)
  (if (atom slug-and-methods)
      ;; Get route by default
      `(app-add-route
        ,app
        ,slug-and-methods
        '(:get)
        (defun ,(intern (nest.route::name-route slug-and-methods '(:get))) ()
          ,@body))

      ;; Route with methods specified
      `(app-add-route
        ,app
        ,(car slug-and-methods)
        ',(rest slug-and-methods)
        (defun ,(intern (nest.route::name-route (car slug-and-methods) (rest slug-and-methods))) ()
          ,@body))))

(defun route-parameter (key &optional default)
  (getf (request:route-parameters) key default))

(defun app-relative-pathname (name)
  "try to return the most meaningful path for the running app."
  (flet ((current-working-directory ()
           #+sbcl (merge-pathnames
                   name
                   ;; Without this extra slash, the last directory of the cwd
                   ;; gets interpreted as the "name" part of a pathname
                   (concatenate 'string (sb-posix:getcwd) "/"))

           #-sbcl nil)
         (app-directory ()
           #+asdf
           (when (and *current-app* (slot-boundp *current-app* 'nest.app::app-package))
             (handler-case
                 (asdf:system-relative-pathname
                  (asdf:find-system (package-name (slot-value *current-app* 'nest.app::app-package)))
                  name)
               (asdf/find-system::missing-component ()
                 nil)))
           #-asdf nil))
    (or (app-directory) (current-working-directory))))
