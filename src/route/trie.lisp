(defpackage #:nest.route.trie
  (:use #:cl #:alexandria)
  (:export #:trie
           #:build-trie
           #:add-route
           #:find-node)
  (:documentation ""))

(in-package #:nest.route.trie)

(defstruct (trie (:constructor make-trie))
  "Trie data structure for storing routing information.
* `node` stores the sequence to match or * for a parameter.
* `branches` stores the sub tries
* `function` stores the function to call if the node matches
* `parameter-name` stores the name of the parameter if there is one."
  (node "" :type string)
  (branches nil :type list)
  (function nil :type (or null function symbol))
  (parameter-name nil :type (or null keyword)))

;;; Functions for captures

(defun maybe-chunk (string)
  "Look for the special characters and return the name of a parameter if there is one.

Returns `(values capture-p capture-name-or-not-special-string remainder)`"

  (if (char= (char string 0) #\:)
      ;; Find the special characters . or /
      (multiple-value-bind (parameter-name remainder) (read-to-special-characers string)
        ;; chop off the :
        (values t (subseq parameter-name 1) remainder))

      ;; Not a capture, nothing special
      (let ((capture-start (position #\: string)))
        (values nil
                (subseq string 0 capture-start)
                (if capture-start
                    (subseq string capture-start)
                    "")))))

(defun read-to-special-characers (string)
  "Read up to a special character (#\. or #\/) and return the match and the remainder."
  (let ((pos (or (position #\. string) (position #\/ string))))
        ;; Return what comes before and after the special character
    (values (subseq string 0 pos) (if pos
                                      (subseq string pos)
                                      ""))))

;;; Trie predicates

(defun capture-p (trie)
  "Indicates the node is a capture node and will consume anything up to a #\. or #\/ or the end of the query."

  (string= (trie-node trie) "*"))

(defun root-p (trie)
  "The root node of a trie has no string in it's node."

  (string= (trie-node trie) ""))

(defun leaf-p (trie)
  "Indicates the node has no childern, i.e. is a leaf node."

  (not (trie-branches trie)))

(defun match-p (trie query)
  "Returns (values match-p value)."

  (if (capture-p trie)
      ;; Node captures - consume a part of query
      (read-to-special-characers query)

      ;; Node is a text node, try to match
      (starts-with-subseq (trie-node trie) query :return-suffix t)))

;;; Trie building

(defun build-trie (routes)
  "Expects an association list of shape (slug . funcallable) that will be assembled into a routing trie."
  (let ((root (make-trie)))
    (loop for (slug . funcallable) in routes
       do (add-route root slug funcallable))
    root))

(defun sort-trie (trie)
  (setf (trie-branches trie)
        (stable-sort (trie-branches trie) #'string-greaterp :key #'trie-node))
  trie)

(defun common-prefix (string1 string2)
  (declare (optimize speed space))
  (loop
     with len1 = (length string1)
     with len2 = (length string2)
     for i from 0 below (min len1 len2)
     until (char/= (char string1 i) (char string2 i))

     finally (return
               (values
                (if (zerop i)
                    nil
                    (subseq string1 0 i))
                (= len1 i)
                (= len2 i)))))

(defun maybe-split-parent (parent-trie new-trie)
  "Possibly split the parent trie into a common token node
  with two childern, one: the new node, the other: the remainder
  of the parent node's path."
  (declare (optimize space speed))

  ;; (format t "~&(m-s-p): ~a ~a" parent-trie new-trie)
  (flet ((clear-trie (trie)
           (setf
            ;; Clear other fields of the trie
            (trie-function       trie) nil
            (trie-parameter-name trie) nil)
           trie)

         (copy-trie-in-place (to from)
           (setf (trie-node to) (trie-node from)
                 (trie-function to) (trie-function from)
                 (trie-parameter-name to) (trie-parameter-name from)
                 (trie-branches to) (trie-branches from))))

    (trivia:multiple-value-match (common-prefix (trie-node parent-trie) (trie-node new-trie))
      ;; This is strictly a subroute, it belongs in the parent's branches
      ((nil _ _)
       ;; (format t "~&Adding trie ~S to ~S" (trie-node new-trie) (trie-node parent-trie))
       (push new-trie (trie-branches parent-trie)))

      ;; The nodes share a common prefix but are otherwise different
      ((prefix nil nil)
       (declare (type simple-string prefix))
       ;; (format t "~&Common prefix (p nil nil): ~S" prefix)
       (let ((new-sibling   (copy-trie parent-trie))
             (sibling-node  (subseq (trie-node parent-trie) (length prefix)))
             (new-trie-node (subseq (trie-node new-trie) (length prefix))))
         ;; Re-node everyone
         (setf (trie-node new-trie) new-trie-node
               (trie-node new-sibling) sibling-node
               (trie-node parent-trie) prefix
               ;; New parent gets the two nodes as childern
               (trie-branches parent-trie) (list new-trie new-sibling))
         (clear-trie parent-trie)))

      ;; The parent node is a prefix of the new child
      ((prefix t nil)
       (declare (type simple-string prefix))
       ;; (format t "~&Case (p t nil): ~S" prefix)
       (setf (trie-node new-trie) (subseq (trie-node new-trie) (length prefix)))
       (push new-trie (trie-branches parent-trie)))

      ;; The new trie is a prefix of the parent (or the first node is, in case of
      ;; a remainder string with a capture part)
      ((prefix nil t)
       (declare (type simple-string prefix))
       ;; (format t "~&Case (p nil t): ~S" prefix)
       (let ((new-child (copy-trie parent-trie)))
         ;; new-child is the original parent, but knocked down a level and made a child
         ;; of new-trie, which it turns out was it's parent

         ;; remove prefix
         (setf (trie-node new-child) (subseq (trie-node new-child) (length prefix)))

         ;; Copy the new trie into place as the new parent node
         (copy-trie-in-place parent-trie new-trie)
         ;; parent-trie is now the correct thing, don't use new-trie anymore

         ;; Add the copy of the old parent to parent-trie's branches
         (push new-child (trie-branches parent-trie))))

      ;; The new trie equals the parent, copy appropriate properties
      ((_ t t)
       ;; (format t "~&Copying trie ~a into ~a" new-trie parent-trie)
       (when (trie-function parent-trie)
         (warn "Overriding trie-function in ~a." parent-trie))
       (when (trie-parameter-name parent-trie)
         (warn "Overriding trie-parameter-name in ~a." parent-trie))

       (setf (trie-function parent-trie) (trie-function new-trie)
             (trie-parameter-name parent-trie) (trie-parameter-name new-trie))))))

(defun maybe-capture-trie (path function)
  "Create at least one but as many tries as required to encode a path
  that might contain captures."
  (multiple-value-bind (capture-p match remainder) (maybe-chunk path)
    (make-trie
     :node (if capture-p "*" match)
     :parameter-name (when capture-p
                       (if (keywordp match)
                           match
                           (make-keyword (string-upcase match))))
     :branches (unless (emptyp remainder)
                 ;; If the string contains more captures,
                 ;; we need to make more nodes
                 (list (maybe-capture-trie remainder function)))
     :function (when (emptyp remainder)
                 function))))

(defun add-route (trie route function)
  ;; Fetch the closest extant node so we can build new branches from there
  (multiple-value-bind (closest-node _ remainder unique-p) (find-closest-node trie route)
    (declare (ignore _))

    ;; (format t "~&(add-route): ~a ~a~%" closest-node remainder)

    (if (emptyp remainder)
        ;; The node already existed, assign the function and don't make any
        ;; new nodes
        (setf (trie-function closest-node) function)

        (let ((new-node (maybe-capture-trie remainder function)))
          (if unique-p
              (push new-node (trie-branches closest-node))
              (maybe-split-parent closest-node new-node))))
    (sort-trie closest-node)
    ;; Finally, return the root node
    trie))

;;; Finding nodes in the trie

(defun find-node (trie query)
  "Finds the node matching the query string in the tree, if there is one.

Returns `(values match-p funcallable parameters)`."

  (multiple-value-bind (trie captures remainder)
      (find-closest-node trie query)

    (if (emptyp remainder)
        (values trie (trie-function trie) captures)
        (values nil nil nil))))

(defun find-closest-node (trie query)
  "Find the node that is the closest match for the query string.  Returns `(values trie captures remainder remainder-unique-p)`

`remainder-unique-p` indicates that the remainder comes after the `closest-node`."
  (declare (optimize speed space))
  (let ((nodes (copy-list (trie-branches trie)))
        (closest-node trie)
        captures
        (remainder query))

    (flet ((maybe-enter-branch (trie subquery)
             (multiple-value-bind (match match-remainder)
                 (match-p trie subquery)
               ;; (format t "~&(m-e-b): ~a ~a~%" match match-remainder)

               (if match
                   (progn
                     ;; Capture if it's a capture trie and then move on
                     (when (capture-p trie)
                       (setf (getf captures (alexandria:make-keyword (trie-parameter-name trie)))
                             match))

                     ;; Update variables to point to the new node
                     (setf remainder match-remainder
                           closest-node trie)
                     (setf nodes (trie-branches trie))

                     ;; Return matched string portion
                     match)

                   (when (common-prefix (trie-node trie) subquery)
                       ;; If there's a common substring, that node is closest
                       (return-from find-closest-node (values trie captures subquery nil)))))))
      (loop
         while nodes
         for branch = (pop nodes)
         ;; End iteration over branches if it's time to enter
         ;; one of the branches and search it.
         do (maybe-enter-branch branch remainder)))
    (values closest-node captures remainder t)))
