(defpackage #:nest.mail
  (:use #:alexandria #:cl)
  (:import-from #:nest.configuration
                #:config)
  (:export #:send-message))

(in-package #:nest.mail)

(defun textify-html (string)
  (flet ((mangle-linebreaks (string)
           (ppcre:regex-replace-all "<br*(/)?>" string #.(coerce '(#\Newline) 'string)))

         ;; http://stackoverflow.com/questions/9503323/replace-hyperlinks-with-plain-text-followed-by-url-in-brackets-using-c-sharp
         (mangle-links (string)
           (ppcre:regex-replace-all "<a\\s+href\\s*=\\s*[\"']([^\"']*)[\"'].*?>(.*)</a>"
                                    string
                                    (lambda (string s e ms me rss res)
                                      (declare (ignore s e ms me))
                                      (format nil "~a (~a)"
                                              (subseq string (svref rss 1) (svref res 1))
                                              (subseq string (svref rss 0) (svref res 0))))))

         (mangle-headers (string)
           (ppcre:regex-replace-all "<h[1234567][^>]*>(.*)</h[1234567][^>]*>"
                                    string
                                    (lambda (string s e ms me rss res)
                                      (declare (ignore s e ms me))
                                      (format nil "~a~%~v@{~A~:*~}~%"
                                              (subseq string (svref rss 0) (svref res 0))
                                              (- (svref res 0) (svref rss 0))
                                              #\=))))

         (mangle-paragraphs (string)
           (ppcre:regex-replace-all "<p[^>]*>(.*)</p[^>]*>"
                                    string
                                    (lambda (string s e ms me rss res)
                                      (declare (ignore s e ms me))
                                      (format nil "~a~%"
                                              (subseq string (svref rss 0) (svref res 0))))))

         (remove-head (string)
           (ppcre:regex-replace-all "<head[^>]*>*</head[^>]*>" string ""))

         (remove-all-tags (string)
           (ppcre:regex-replace-all "<[^>]*>" string "")))

    (remove-all-tags
     (mangle-links
      (mangle-headers
       (mangle-linebreaks
        (mangle-paragraphs
         (remove-head string))))))))

(defun send-message (template to subject
                     &rest args
                     &key cc bcc attachments
                     &allow-other-keys)
  (let* ((html-message (apply #'nest.templates::render-template
                              template
                              (remove-from-plist args :cc :bcc :attachments)))
         (text-message (textify-html html-message)))
    (cl-smtp:send-email
     (config :mail-host)
     (config :mail-from)
     to
     subject
     text-message
     :ssl (config :mail-ssl :tls)
     :cc cc
     :bcc bcc
     :attachments attachments
     :display-name (config :mail-display-name)
     :html-message html-message
     :authentication (list (config :mail-username) (config :mail-secret)))))
