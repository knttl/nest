# Nest - A simple web framework built on clack.

There are lots of web frameworks.  Check out (Caveman)[https://github.com/fukamachi/caveman] and (Lucerne)[https://github.com/eudoxia0/lucerne] if you haven't already.

Nest attempts to be sort of magic, in the style of rails, but not too magic.  Most web programming is pretty simple, route, validate, respond.  There may be templates, also, but there's might only be one and an API, say if you're writing a single page app.  If you're doing anything beyond that, Nest's opinion is you're probably writing code into your webapp that you should really be writing a library for.


## Current TODOs

- [X] Base app
  - [x] Define views
  - [x] Respond to things
- [x] Sessions (just use lack middlewares)
- [~] Configuration management (started, still clunky)
- [ ] Templates
  - [ ] Pass some global variables into all templates
- [ ] Asset management
  - [ ] Production cache busting
  - [x] Functions to serve files safeishly
- [ ] Error handling
- [ ] CRUD
- [ ] Validation
- [ ] Database smartiness

## Installation

Hopefully, someday I'll feel good enough about this to submit it for inclusion in quicklisp.  Until then, clone it to your `~/quicklisp/local-projects/` or somewhere equivalent where `ASDF` can find it.

## Author

* Matt Novenstern (fisxoj@gmail.com)

## Copyright

Copyright (c) 2016 Matt Novenstern (fisxoj@gmail.com)

## License

Licensed under the MIT License.
