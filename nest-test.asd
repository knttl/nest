#|
  This file is a part of nest project.
  Copyright (c) 2016 Matt Novenstern (fisxoj@gmail.com)
|#

(in-package :cl-user)
(defpackage nest-test-asd
  (:use :cl :asdf))
(in-package :nest-test-asd)

(defsystem nest-test
  :author "Matt Novenstern"
  :license "MIT"
  :depends-on (:nest
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "nest")
                 (:test-file "response")
                 (:test-file "route/trie")
                 (:test-file "route")
                 (:test-file "configuration"))))
  :description "Test system for nest"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
